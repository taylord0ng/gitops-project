# gitops-project

## About

This is a [manifest repository example project](https://docs.gitlab.com/ee/user/clusters/agent/#creating-a-manifestyaml) of the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/).

## Setup

To use this project correctly, please follow the entire setup example in the [GitLab Kubernetes Agent documentation](https://docs.gitlab.com/ee/user/clusters/agent/) .

At a minimum, a manifest repository should contain a `manifest.yaml` that the Agent will monitor for any changes.

Once changes are merged to this file the Agent will apply them to your cluster.

The Agent also requires a configuration repository to function. [This example](https://gitlab.com/gitlab-org/configure/examples/kubernetes-agent/) is paired with this manifest repository.